#!/bin/bash
echo $PWD
git log -1

PROJECT="$1"
BASE_OUTPUT=/var/www/html/${PROJECT}
VERSION=$(git describe --always)
OUTPUT=${BASE_OUTPUT}/$(date +"%Y%m%d_%H%M%S")_${VERSION}

echo "Will build ${PROJECT} ${VERSION}"


echo "Virtualenv ..."
rm -rf venv-doc
if ! virtualenv -p python3 venv-doc ; then
	echo "Virtualenv failed"
	exit 1
fi
source venv-doc/bin/activate

echo "Installing sphinx ..."
if ! pip3 install sphinx || ! pip3 install sphinxcontrib-plantuml ; then
	echo "Sphinx install failed"
	exit 1
fi

echo "Building ..."
if ! make clean ; then
	echo "Build failed"
	exit 1
fi

echo "Building doc ..."
if ! make doc ; then
	echo "Doc Build failed"
	exit 1
fi

echo "Cleaning"
make uninstall_py

if ! [ -d doc/build ]; then
	echo "No doc generated"
	exit 0
fi

echo "Copying ..."

mv -v doc/build ${OUTPUT}
rm -f ${BASE_OUTPUT}/latest
ln -s $(basename ${OUTPUT}) ${BASE_OUTPUT}/latest

chmod -R a+rX ${BASE_OUTPUT}
echo "All done"

