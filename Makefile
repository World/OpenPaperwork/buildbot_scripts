SCRIPTS=$(wildcard *.sh)

build:

install:

uninstall:

build_py:

build_c:

version:

doc:

check_%:
	bash -n $(@:check_%=%)

check: $(SCRIPTS:%=check_%)

test:

linux_exe:

windows_exe:

release:

clean:

install_py:

install_c:

uninstall_py:

uninstall_c:

help:
	@echo "make build || make build_py"
	@echo "make check"
	@echo "make doc"
	@echo "make help: display this message"
	@echo "make install || make install_py"
	@echo "make release"
	@echo "make test"
	@echo "make uninstall || make uninstall_py"

.PHONY: \
	build \
	build_c \
	build_py \
	check \
	doc \
	linux_exe \
	windows_exe \
	help \
	install \
	install_c \
	install_py \
	release \
	test \
	uninstall \
	uninstall_c \
	version
