#!/bin/bash

echo SHELL: $SHELL
echo BASH_VERSION: $BASH_VERSION

exec 2>&1

REPOSITORY="$1"
BRANCH="$2"
PROJECT="$3"

if [ -z "${PROJECT}" ] ; then
    echo "Missing argument:"
    echo " <repository> <branch> <project>"
    exit 1
fi

if [ -z "${WIN_BUILD_IP}" ] ; then
    WIN_BUILD_IP=192.168.42.22
fi
if [ -z "${WIN_BUILD_USER}" ] ; then
    WIN_BUILD_USER=jflesch
fi
if [ -z "${SSH_KEY}" ] ; then
    SSH_KEY=/var/lib/buildbot/id_w10_rsa
fi

function wssh {
    echo "####" ssh -i ${SSH_KEY} ${WIN_BUILD_USER}@${WIN_BUILD_IP} \
     "export MSYSTEM=MINGW32 ; source /etc/profile ; " \
     "$@"
    ssh -i ${SSH_KEY} ${WIN_BUILD_USER}@${WIN_BUILD_IP} \
     "export MSYSTEM=MINGW32 ; source /etc/profile ; " \
     "$@"
    res=$?
    if [ $res -ne 0 ] ; then
        echo "Command failed"
        exit 1
    fi
    return $res
}

function wscp {
    echo scp -i ${SSH_KEY} "$@"
    scp -i ${SSH_KEY} "$@"
    res=$?
    if [ $res -ne 0 ] ; then
        echo "Command failed"
        exit 1
    fi
    return $res
}

wssh "rm -rf ~/testhw ; mkdir -p ~/testhw"
wssh "git clone --depth 1 -b ${BRANCH} ${REPOSITORY} ~/testhw/git"
wssh "cd ~/testhw/git ; make test install PREFIX=~/testhw/install"
wssh "cd ~/testhw/install/bin ; export LD_LIBRARY_PATH=~/testhw/install/bin ; export GI_TYPELIB_PATH=~/testhw/install/lib/girepository-1.0 ; ~/testhw/git/subprojects/libinsane-gobject/tests/test_hw.py ~/testhw/out"

exit 0
