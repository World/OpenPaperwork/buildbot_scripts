#!/bin/bash
cd /var/lib/buildbot/scripts

echo "Updating"
flock -x .git/script.lock git pull

exit $?
