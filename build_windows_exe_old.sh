#!/bin/bash

echo SHELL: $SHELL
echo BASH_VERSION: $BASH_VERSION

exec 2>&1

REPOSITORY="$1"
BRANCH="$2"
PROJECT="$3"
REVISION="$4"

if [ "${REVISION}" = "latest" ] ; then
    REVISION=""
fi

if [ -z "${PROJECT}" ] ; then
    echo "Missing argument:"
    echo " <repository> <branch> <project> [<revision>]"
    exit 1
fi

WIN_BUILD_IP=192.168.126.91
WIN_BUILD_USER=Jflesch
WIN_BUILD_PASSWD=builder
LOCAL_TMP_DIR=/var/lib/buildbot/git-tmp
# Samba is used to bind /var/lib/buildbot to z:\
WIN_BUILD_TMP_DIR="z:\git-tmp"
SMB_ADDRESS=192.168.126.2
NET_USE_CMD="net use z: \\\\${SMB_ADDRESS}\\workdir /user:buildbot buildbot"

SSH_KEY=/var/lib/buildbot/id_w10_rsa
SSH_PASSWD=builder

TARGET_DL_HOST=windows@192.168.124.19
TARGET_DL_DIR=/var/www/html/windows/amd64

function wssh {
    echo sshpass -p xxxx -- ssh -i ${SSH_KEY} ${WIN_BUILD_USER}@${WIN_BUILD_IP} "$@"
    sshpass -p ${SSH_PASSWD} -- ssh -i ${SSH_KEY} ${WIN_BUILD_USER}@${WIN_BUILD_IP} "$@"
    res=$?
    if [ $res -ne 0 ] ; then
        echo "Command failed"
    fi
    return $res
}

function wscp {
    echo sshpass -p xxxx -- scp -i ${SSH_KEY} "$@"
    sshpass -p ${SSH_PASSWD} -- scp -i ${SSH_KEY} "$@"
    res=$?
    if [ $res -ne 0 ] ; then
        echo "Command failed"
        exit 1
    fi
    return $res
}

# rm -rf ${LOCAL_TMP_DIR}
mkdir -p ${LOCAL_TMP_DIR}
cd ${LOCAL_TMP_DIR}

if ! [ -d "${LOCAL_TMP_DIR}/${PROJECT}" ] ; then
    if ! git clone ${REPOSITORY} "${LOCAL_TMP_DIR}/${PROJECT}" ; then
        echo "Git clone failed"
        exit 1
    fi
else
    (cd "${LOCAL_TMP_DIR}/${PROJECT}" && git checkout master)
    if ! (cd "${LOCAL_TMP_DIR}/${PROJECT}" && git pull) ; then
        echo "Git pull failed"
        exit 1
    fi
fi

if ! (cd "${LOCAL_TMP_DIR}/${PROJECT}" && git checkout ${BRANCH} && git reset --hard) ; then
    echo "Git branch ${BRANCH} failed"
    exit 1
fi

if [ -n "${REVISION}" ] && ! (cd "${LOCAL_TMP_DIR}/${PROJECT}" && git checkout ${REVISION} && git reset --hard) ; then
    echo "Git checkout ${REVISION} failed"
    exit 1
fi

VERSION=$(cd "${LOCAL_TMP_DIR}/${PROJECT}" && git describe --always)

if ! (cd "${LOCAL_TMP_DIR}/${PROJECT}" && make clean) ; then
    echo "Make clean failed"
    exit 1
fi

# 'make version' doesn't work on Windows
if ! (cd "${LOCAL_TMP_DIR}/${PROJECT}" && make version) ; then
    echo "Make version failed"
    exit 1
fi

chown -R buildbot:buildbot ${LOCAL_TMP_DIR}

cat << EOF | unix2dos > build-${PROJECT}.bat
@echo on
set PYTHON=python
${NET_USE_CMD}
z:
cd ${WIN_BUILD_TMP_DIR}\\${PROJECT}
make uninstall MAKE=make
make install PYTHON=python MAKE=make
make windows_exe PYTHON=python MAKE=make
EOF

wscp build-${PROJECT}.bat ${WIN_BUILD_USER}@${WIN_BUILD_IP}:/users/${WIN_BUILD_USER}

if ! wssh "cmd /c C:\\users\\${WIN_BUILD_USER}\\build-${PROJECT}.bat" ; then
    echo "Build failed"
    exit 1
fi

echo "Looking for result executable in ${LOCAL_TMP_DIR}/${PROJECT}/"
exe_found=0
for dist in $(find "${LOCAL_TMP_DIR}/${PROJECT}/" -type d -iname dist) ; do
    echo "- Looking for result executable in ${dist}/"
    for result_exe in $(find ${dist} -type f -iname \*exe) ; do
        echo "Result executable: ${result_exe}"
        exe_found=1
        result_dir=$(dirname ${result_exe})
        rm -f ${result_dir}/${PROJECT}-*.zip
        if [ $(find ${result_dir} | grep -v .egg | wc -l) -gt 2 ]; then
            echo "Zipping"
            if ! (cd ${result_dir} ; zip -r ${PROJECT}-${BRANCH}-${VERSION}.zip *) ; then
                echo "Zip failed"
                exit 1
            fi
            if ! scp -i ${SSH_KEY} ${result_dir}/${PROJECT}-${BRANCH}-${VERSION}.zip ${TARGET_DL_HOST}:${TARGET_DL_DIR} ; then
                echo "scp failed"
                exit 1
            fi
            if ! ssh -i ${SSH_KEY} ${TARGET_DL_HOST} \
                    ln -nfs ${TARGET_DL_DIR}/${PROJECT}-${BRANCH}-${VERSION}.zip \
                    ${TARGET_DL_DIR}/${PROJECT}-${BRANCH}-latest.zip ; then
                echo "ln -nfs failed"
                exit 1
            fi
        else
            if ! scp -i ${SSH_KEY} ${result_exe} ${TARGET_DL_HOST}:${TARGET_DL_DIR}/${PROJECT}-${BRANCH}-${VERSION}.exe ; then
                echo "scp failed"
                exit 1
            fi
            if ! ssh -i ${SSH_KEY} ${TARGET_DL_HOST} \
                    ln -nfs ${TARGET_DL_DIR}/${PROJECT}-${BRANCH}-${VERSION}.exe \
                    ${TARGET_DL_DIR}/${PROJECT}-${BRANCH}-latest.exe ; then
                echo "ln -nfs failed"
                exit 1
            fi
        fi
        if ! ssh -i ${SSH_KEY} ${TARGET_DL_HOST} \
                chmod -R a+rX ${TARGET_DL_DIR} ; then
            echo "chmod failed"
            exit 1
        fi
    done
done

if [ ${exe_found} -eq 0 ] ; then
    echo "exe not found"
    exit 1
fi

echo "Version successfully build: ${PROJECT}-${VERSION}"
