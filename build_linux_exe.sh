#!/bin/bash
echo $PWD
git log -1

REPOSITORY="$1"
BRANCH="$2"
PROJECT="$3"

BASE_OUTPUT=/var/lib/buildbot/export/${PROJECT}
BASE_TMP=/var/lib/buildbot/tmp
PYINSANE_CHECKOUT=${BASE_TMP}/pyinsane

VERSION=$(git describe --always)
OUTPUT=${BASE_OUTPUT}/${VERSION}-${BRANCH}

echo "Will build ${PROJECT} ${VERSION}"

echo "Building venv ..."
rm -rf venv
if ! virtualenv -p python3 --system-site-packages venv ; then
	echo "Failed to create venv"
	exit 1
fi
source venv/bin/activate

echo "Installing latest Pyinsane ..."
mkdir -p "${BASE_TMP}"
rm -rf "${PYINSANE_CHECKOUT}"

pushd "${BASE_TMP}"
git clone https://gitlab.gnome.org/World/OpenPaperwork/pyinsane.git
pushd pyinsane
make clean
if ! make install ; then
    echo "Pyinsane make install failed"
    exit 1
fi
popd
popd


echo "Installing pyinstaller ..."
pip3 install pyinstaller

echo "Building project ..."
if ! make install || ! make linux_exe ; then
	echo "Build failed"
	exit 1
fi

echo "Copying ..."
rm -rf dist/*.egg

if ! mkdir -p ${OUTPUT} ; then
	echo "mkdir failed"
	exit 1
fi

echo ln -nfs $(basename ${OUTPUT}) ${BASE_OUTPUT}/latest-${BRANCH}
ln -nfs $(basename ${OUTPUT}) ${BASE_OUTPUT}/latest-${BRANCH}

for fn in dist/* ; do
	echo mv -v ${fn} ${OUTPUT}/$(basename ${fn})_${VERSION}
	mv -v ${fn} ${OUTPUT}/$(basename ${fn})_${VERSION}
	echo ln -nfs $(basename ${fn})_${VERSION} ${OUTPUT}/$(basename ${fn})
	ln -nfs $(basename ${fn})_${VERSION} ${OUTPUT}/$(basename ${fn})
done
chmod -R a+rX ${BASE_OUTPUT}

echo "All done"
