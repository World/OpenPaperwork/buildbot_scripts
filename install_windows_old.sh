#!/bin/bash

echo SHELL: $SHELL
echo BASH_VERSION: $BASH_VERSION

exec 2>&1

REPOSITORY="$1"
BRANCH="$2"
PROJECT="$3"

if [ -z "${PROJECT}" ] ; then
    echo "Missing argument:"
    echo " <repository> <branch> <project>"
    exit 1
fi

WIN_BUILD_IP=192.168.126.91
WIN_BUILD_USER=Jflesch
WIN_BUILD_PASSWD=builder
LOCAL_TMP_DIR=/var/lib/buildbot/git-tmp
# Samba is used to bind /var/lib/buildbot to z:\
WIN_BUILD_TMP_DIR="z:\git-tmp"
SMB_ADDRESS=192.168.126.2
NET_USE_CMD="net use z: \\\\${SMB_ADDRESS}\\workdir /user:buildbot buildbot"

SSH_KEY=/var/lib/buildbot/id_w10_rsa
SSH_PASSWD=builder

TARGET_DL_HOST=windows@192.168.124.19
TARGET_DL_DIR=/var/www/html/windows/amd64

function wssh {
    echo sshpass -p xxxx -- ssh -i ${SSH_KEY} ${WIN_BUILD_USER}@${WIN_BUILD_IP} "$@"
    sshpass -p ${SSH_PASSWD} -- ssh -i ${SSH_KEY} ${WIN_BUILD_USER}@${WIN_BUILD_IP} "$@"
    res=$?
    if [ $res -ne 0 ] ; then
        echo "Command failed"
    fi
    return $res
}

function wscp {
    echo sshpass -p xxxx -- scp -i ${SSH_KEY} "$@"
    sshpass -p ${SSH_PASSWD} -- scp -i ${SSH_KEY} "$@"
    res=$?
    if [ $res -ne 0 ] ; then
        echo "Command failed"
        exit 1
    fi
    return $res
}

# rm -rf ${LOCAL_TMP_DIR}
mkdir -p ${LOCAL_TMP_DIR}
cd ${LOCAL_TMP_DIR}

if ! [ -d "${LOCAL_TMP_DIR}/${PROJECT}" ] ; then
    if ! git clone ${REPOSITORY} "${LOCAL_TMP_DIR}/${PROJECT}" ; then
        echo "Git clone failed"
        exit 1
    fi
else
    if ! (cd "${LOCAL_TMP_DIR}/${PROJECT}" && git pull) ; then
        echo "Git pull failed"
        exit 1
    fi
fi

if ! (cd "${LOCAL_TMP_DIR}/${PROJECT}" && git checkout ${BRANCH} && git reset --hard) ; then
    echo "Git branch failed"
    exit 1
fi

VERSION=$(cd "${LOCAL_TMP_DIR}/${PROJECT}" && git describe --always)

if ! (cd "${LOCAL_TMP_DIR}/${PROJECT}" && make clean) ; then
    echo "Make clean failed"
    exit 1
fi

# 'make version' doesn't work on Windows
if ! (cd "${LOCAL_TMP_DIR}/${PROJECT}" && make version) ; then
    echo "Make version failed"
    exit 1
fi

chown -R buildbot:buildbot ${LOCAL_TMP_DIR}

cat << EOF | unix2dos > build-${PROJECT}.bat
@echo on
set PYTHON=python
${NET_USE_CMD}
z:
cd ${WIN_BUILD_TMP_DIR}\\${PROJECT}
make uninstall MAKE=make
make install PYTHON=python MAKE=make
EOF

wscp build-${PROJECT}.bat ${WIN_BUILD_USER}@${WIN_BUILD_IP}:/users/${WIN_BUILD_USER}

if ! wssh "cmd /c C:\\users\\${WIN_BUILD_USER}\\build-${PROJECT}.bat" ; then
    echo "Build failed"
    exit 1
fi

echo "Version successfully installed: ${PROJECT}-${VERSION}"
exit 0
